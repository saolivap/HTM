import ROOT 
ROOT.gROOT.SetBatch(True)

stylepath = '/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/'

ROOT.gROOT.LoadMacro( stylepath + 'AtlasStyle.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasLabels.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasUtils.C')
ROOT.SetAtlasStyle()
from ROOT import gPad
from ROOT import TCanvas, TColor, TGaxis, TH1F, TPad

#Flags
doLogX = False
doLogY = False

basepath = '/home/saolivap/WorkArea/GitLab/HTM/build/'
tree = 'trip'
varias = ['mA', 'mch', 'mdelta', 'mA_approx', 'mch_approx', 'mdelta_approx']
ratios = ['ratioMa', 'ratioMch', 'ratioMd']
params = ['v1', 'v2', 'v3', 'L1', 'L2','L3', 'L4', 'L5', 'B1', 'B2', 'B3', 'k']
plots = ['oddMass', 'chargeMass', 'doubleMass']
 
sample = basepath + 'trip.root'

outfile = ROOT.TFile('/home/saolivap/WorkArea/GitLab/HTM/util/hist.root','RECREATE')

# CREATING HISTOGRAMS
h = {}
for param in params:
    h[param] = {}

    for varia in varias:
        h[param][varia] = {}

        bins = 1000
        if param == 'v1':
            xmin = pow(10,-5)
            xmax = pow(10,3)
        elif param == 'v2':
            xmin = 246.1
            xmax = 246.3
        elif param == 'v3':
            xmin = pow(10,-5)
            xmax = 0.35
        elif param == 'L5':
            xmin = -12
            xmax = 0
        elif param == 'k':
            xmin = pow(10,-10)
            xmax = 0.5*pow(10,-3)        
        else:
            xmin = -1
            xmax = 1
            
        if varia == 'mh1':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mh2':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mh3':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mA':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mch':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mdelta':
            ymin = pow(10,-5)
            ymax = 5000
        else:
            ymin = -1
            ymax = 1
            
        n = 'h_%s_%s' % (param, varia)
        h[param][varia] = ROOT.TH2F(n, n + "; %s (GeV); %s (GeV)" % (varia, param), bins, xmin, xmax, bins, ymin, ymax) 
        h[param][varia].SetDirectory(outfile)

# Ratio histograms:
h_ratio = {}
for param in params:
    h_ratio[param] = {}

    for ratio in ratios:
        h_ratio[param][ratio] = {}        
        n = 'h_ratio_%s_%s' % (param, ratio)
        
        ymin = 0.6
        ymax = 1.4

        if param == 'v1' or param == 'v2' or param == 'v3':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, -0.01, 0.01, 1000, ymin, ymax) 
        elif param == 'e1' or param == 'e2' or param == 'e3':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, -1.0, 1.0, 1000, ymin, ymax) 
        elif param == 'B1' or param == 'B2' or param == 'B3':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, -200.0, 200.0, 1000, ymin, ymax)
        elif param == 'mh2':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, 385.0, 1285.0, 1000, ymin, ymax)
        elif param == 'h1':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, -0.0026, 0.0024, 1000, ymin, ymax)
        elif param == 'h2':
            h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, 0.0156, 0.0207, 1000, ymin, ymax) 
        else: h_ratio[param][ratio] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (ratio, param), 1000, -1, 1, 1000, ymin, ymax) 
        h_ratio[param][ratio].SetDirectory(outfile)

# For Legend:
g = {}
for hist in hists:
    g[hist] = {}
    n = 'h_%s' % (hist)
    g[hist] = ROOT.TH1F(n, n + "; %s [GeV]; Events" % (hist), 1000, 0, 1)
    g[hist].SetDirectory(outfile)
    
# FILLING
f = ROOT.TFile.Open(sample)
t = f.Get(tree)
 
for param in params:
    print 'processing parameter: %s' % param
    
    # 2-D Histograms
    for hist in hists:
        print 'processing histogram: %s' % hist

        if hist == 'BRtau' or hist == 'BRtau0':
            ymin = 0.10729
            ymax = 0.1075
        elif hist == 'BRmu' or hist == 'BRmu0':
            ymin = 0.00026
            ymax = 0.00051
        else:
            ymin = 1
            ymax = 100
            
        if param == 'v1' or param == 'v2' or param == 'v3':
            n = 'htmp(1000,-0.01,0.01,1000,%f,%f)' % (ymin,ymax) 
        elif param == 'e1' or param == 'e2' or param == 'e3':
            n = 'htmp(1000,-1.0,1.0,1000,%f,%f)' % (ymin,ymax) 
        elif param == 'B1' or param == 'B2' or param == 'B3':
            n = 'htmp(1000,-200.0,200.0,1000,%f,%f)' % (ymin,ymax)
        elif param == 'mh2':
            n = 'htmp(1000,385.0,1285.0,1000,%f,%f)' % (ymin,ymax)
        elif param == 'h1':
            n = 'htmp(1000,-0.0026,0.0024,1000,%f,%f)' % (ymin,ymax)
        elif param == 'h2':
            n = 'htmp(1000,0.0156,0.0207,1000,%f,%f)' % (ymin,ymax)
        else: n = 'htmp(1000,-1,1,1000,%f,%f)' % (ymin,ymax) 
        if t.Draw('%s:%s>>%s' % (hist, param, n), 
                  '', 
                  'goff'
        ):
            h[param][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)

    # Ratio Histograms
    for ratio in ratios:
        print 'processing histogram: %s' % ratio

        ymin = 0.6
        ymax = 1.4
        
        if param == 'v1' or param == 'v2' or param == 'v3':
            n = 'htmp(1000,-0.01,0.01,1000,%f,%f)' % (ymin,ymax) 
        elif param == 'e1' or param == 'e2' or param == 'e3':
            n = 'htmp(1000,-1.0,1.0,1000,%f,%f)' % (ymin,ymax) 
        elif param == 'B1' or param == 'B2' or param == 'B3':
            n = 'htmp(1000,-200.0,200.0,1000,%f,%f)' % (ymin,ymax)
        elif param == 'mh2':
            n = 'htmp(1000,385.0,1285.0,1000,%f,%f)' % (ymin,ymax)
        elif param == 'h1':
            n = 'htmp(1000,-0.0026,0.0024,1000,%f,%f)' % (ymin,ymax)
        elif param == 'h2':
            n = 'htmp(1000,0.0156,0.0207,1000,%f,%f)' % (ymin,ymax)
        else: n = 'htmp(1000,-1,1,1000,%f,%f)' % (ymin,ymax)  
        if t.Draw('%s:%s>>%s' % (ratio, param, n), 
                  '', 
                  'goff'
        ):
            h_ratio[param][ratio].Add(ROOT.gDirectory.Get('htmp'), 1.)

            
del f

# HIST, PARAM CONFIG
for hist in hists:
    for param in params:

        h[param][hist].SetMarkerSize(1)
        h[param][hist].SetMarkerStyle(ROOT.kDot)
        h[param][hist].SetStats(0) # No statistics on upper plot
        h[param][hist].GetYaxis().SetTitleSize(20)
        h[param][hist].GetYaxis().SetTitleFont(43)
        h[param][hist].GetYaxis().SetTitleOffset(1.55)
        #for legend
        g[hist].SetMarkerSize(1)
        g[hist].SetMarkerStyle(ROOT.kFullCircle)

        h[param][hist].GetYaxis().SetTitleSize(25)
        h[param][hist].GetYaxis().SetTitleFont(43)
        h[param][hist].GetYaxis().SetLabelSize(25)
        h[param][hist].GetYaxis().SetLabelFont(43)
        if hist == 'BRtau' or hist == 'BRtau0' or hist == 'BRmu' or hist == 'BRmu0':
            h[param][hist].GetYaxis().SetTitle('BR')
        else: h[param][hist].GetYaxis().SetTitle('default')
        if param == 'v1' or param == 'v2' or param == 'v3':
#            h[param][hist].GetXaxis().SetRangeUser(-0.001, 0.001)
            h[param][hist].GetXaxis().SetNdivisions(505)            
#        if param == 'e1' or param == 'e2' or param == 'e3': h[param][hist].GetXaxis().SetRangeUser(-0.05, 0.05)
#        if param == 'B1' or param == 'B2' or param == 'B3': h[param][hist].GetXaxis().SetRangeUser(-20, 20)
#        else: h[param][hist].GetXaxis().SetRangeUser(-1, 1)
        # colour and style
        if hist == 'BRtau' or hist == 'BRmu':
            h[param][hist].SetMarkerColor(ROOT.kRed)
            g[hist].SetMarkerColor(ROOT.kRed)
            
for ratio in ratios:
    for param in params:

        if param == 'lam121': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{121}')
        elif param == 'lam122': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{122}')        
        elif param == 'lam123': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{123}')
        elif param == 'lam131': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{131}')
        elif param == 'lam132': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{132}')
        elif param == 'lam133': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{133}')
        elif param == 'lam231': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{231}')
        elif param == 'lam232': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{232}')
        elif param == 'lam233': h_ratio[param][ratio].GetXaxis().SetTitle('#lambda_{233}')
        elif param == 'v1': h_ratio[param][ratio].GetXaxis().SetTitle('v_{1}')
        elif param == 'v2': h_ratio[param][ratio].GetXaxis().SetTitle('v_{2}')
        elif param == 'v3': h_ratio[param][ratio].GetXaxis().SetTitle('v_{3}')
        elif param == 'e1': h_ratio[param][ratio].GetXaxis().SetTitle('#epsilon_{1}')
        elif param == 'e2': h_ratio[param][ratio].GetXaxis().SetTitle('#epsilon_{2}')
        elif param == 'e3': h_ratio[param][ratio].GetXaxis().SetTitle('#epsilon_{3}')
        elif param == 'B1': h_ratio[param][ratio].GetXaxis().SetTitle('B_{1}')
        elif param == 'B2': h_ratio[param][ratio].GetXaxis().SetTitle('B_{2}')
        elif param == 'B3': h_ratio[param][ratio].GetXaxis().SetTitle('B_{3}')
        elif param == 'mh2': h_ratio[param][ratio].GetXaxis().SetTitle('m_{H}')
        elif param == 'h1': h_ratio[param][ratio].GetXaxis().SetTitle('h_{1}')
        elif param == 'h2': h_ratio[param][ratio].GetXaxis().SetTitle('h_{2}')


        h_ratio[param][ratio].SetMarkerSize(1)
        h_ratio[param][ratio].SetMarkerStyle(ROOT.kDot)
        h_ratio[param][ratio].SetTitle("")
        h_ratio[param][ratio].SetStats(0)

        # Adjust y-axis settings
        y = h_ratio[param][ratio].GetYaxis()
        y.SetTitle("ratio")
        y.SetNdivisions(505)
        y.SetTitleSize(25)#was 20
        y.SetTitleFont(43)
        y.SetTitleOffset(1.55)
        y.SetLabelFont(43)
        y.SetLabelSize(20)#was 15
        
        # Adjust x-axis settings
        x = h_ratio[param][ratio].GetXaxis()
        x.SetTitleSize(25)#was 20
        x.SetTitleFont(43)
        x.SetTitleOffset(2.8)
        x.SetLabelFont(43)
        x.SetLabelSize(20)#was 15

# TEXT, LEGEND CONFIG
tex = ROOT.TLatex(0.2, 0.18, "Turtle approved")
tex.SetName('Test2')
tex.SetNDC(True)
tex.SetTextSize(0.04)

#legend configuration
xmin = 0.65
xmax = 0.8
ymin = 0.6
ymax = 0.85
for plot in plots:
    
    legend_mass = ROOT.TLegend(xmin, ymin, xmax, ymax) 
    legend_mass.SetFillColor(0)
    legend_mass.SetBorderSize(0)
    legend_mass.SetTextFont(42)
    legend_mass.SetTextSize(0.04) # was 0.05 
    legend_mass.SetName('turtle')
    legend_mass.SetShadowColor(0)
    legend_mass.SetFillStyle(0) # transparent box
    
    if plot == 'BRtautau':
        legend_mass.AddEntry(g['BRtau0'],'BR(#tau#tau) MSSM', 'p')
        legend_mass.AddEntry(g['BRtau'],'BR(#tau#tau) MSSM + RpV', 'p')
    elif plot == 'BRmumu':
        legend_mass.AddEntry(g['BRmu0'],'BR(#mu#mu) MSSM', 'p')
        legend_mass.AddEntry(g['BRmu'],'BR(#mu#mu) MSSM + RpV', 'p')

# PLOT IN CANVAS
for param in params:

    for plot in plots:
        print 'processing plot: %s' % plot
    
        c = 'c_%s' % (plot)
        pad = 'pad_%s' % (plot)
        pad_ratio = 'pad_ratio_%s' % (plot)
        c = ROOT.TCanvas('c', 'c', 800, 800)
        pad = ROOT.TPad('pad','pad',0,0.3,1,1) 
        pad.SetBottomMargin(0) # Upper and lower plot are joined

        if doLogY == True: pad.SetLogy()
        if doLogX == True: pad.SetLogx()
        
        pad.Draw()
        pad.cd()
        
        if plot == 'BRtautau':
            h[param]['BRtau'].DrawCopy()
            h[param]['BRtau0'].Draw('same')
        elif plot == 'BRmumu':
            h[param]['BRmu'].DrawCopy()
            h[param]['BRmu0'].Draw('same')
                        
        legend_mass.Draw()
        tex.Draw()    
        c.cd()
        pad_ratio = ROOT.TPad('pad_ratio','pad_ratio',0,0.05,1,0.29)
        pad_ratio.SetTopMargin(0)
        pad_ratio.SetBottomMargin(0.2)
        pad_ratio.Draw()

        # to avoid clipping the bottom zero, redraw a small axis
        h[param]['BRtau'].GetYaxis().SetLabelSize(0.0)
        axis = TGaxis(-5, 20, -5, 220, 20, 220, 510, "")
        axis.SetLabelFont(43)
        axis.SetLabelSize(15)
        axis.Draw()
        
        pad_ratio.cd()

        if plot == 'BRtautau':
            h_ratio[param]['BRratioTau'].Draw()
        elif plot == 'BRmumu':
            h_ratio[param]['BRratioMuon'].Draw()
                            
        c.Update()
        c.SaveAs('/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/Andres/util/plots/ratio/%s_%s.png' % (plot,param))


