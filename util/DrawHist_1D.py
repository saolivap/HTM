import ROOT 
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/AtlasLabels.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad

#Flags
doLogX = False
doLogY = True

basepath = '/home/saolivap/WorkArea/GitLab/HTM/build/'
tree = 'trip'
params = ['v1', 'v2', 'v3', 'mh1', 'mh2', 'mh3', 'mA', 'mch', 'mdelta', 'mA_approx', 'mch_approx', 'mdelta_approx', 'dww', 'dhw', 'L1', 'L2','L3', 'L4', 'L5', 'B1', 'B2', 'B3', 'k']
 
sample = basepath + 'trip.root'

outfile = ROOT.TFile('/home/saolivap/WorkArea/GitLab/HTM/util/hist.root','RECREATE')

# CREATING HISTOGRAMS
h = {}
for param in params:
    h[param] = {}

    bins = 100
    if param == 'v1':
        xmin = pow(10,-5)
        xmax = pow(10,3)
    elif param == 'v2':
        xmin = 246.1
        xmax = 246.3
    elif param == 'v3':
        xmin = pow(10,-5)
        xmax = 0.35
    elif param == 'mh1':
        xmin = pow(10,-5)
        xmax = 360
    elif param == 'mh2':
        xmin = pow(10,-5)
        xmax = 2100
    elif param == 'mh3':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mA':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mch':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mdelta':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mA_approx':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mch_approx':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'mdelta_approx':
        xmin = pow(10,-5)
        xmax = 5000
    elif param == 'L5':
        xmin = -12
        xmax = 0
    elif param == 'k':
        xmin = pow(10,-10)
        xmax = 0.5*pow(10,-3)        
    elif param == 'dww':
        xmin = pow(10,-11)
        xmax = pow(10,-6)
    elif param == 'dhw':
        xmin = pow(10,-5)
        xmax = 4.8
    else:
        xmin = -1
        xmax = 1

    n = 'h_%s' % (param)
    h[param] = ROOT.TH1F(n, n + "; %s (GeV)" % (param), bins, xmin, xmax)
    h[param].SetDirectory(outfile)
    
# FILLING
f = ROOT.TFile.Open(sample)
t = f.Get(tree)

# For the moment we have to use this slow fill method (open the tree) because the direct (from below)
# does not have enough precision for the minimum value of a plotted variable (no more that 10-6)
for event in t:

    h['v1'].Fill(event.v1)
    h['v2'].Fill(event.v2)
    h['v3'].Fill(event.v3)
    h['mh1'].Fill(event.mh1)
    h['mh2'].Fill(event.mh2)
    h['mh3'].Fill(event.mh3)
    h['mA'].Fill(event.mA)
    h['mch'].Fill(event.mch)
    h['mdelta'].Fill(event.mdelta)
    h['mA_approx'].Fill(event.mA_approx)
    h['mch_approx'].Fill(event.mch_approx)
    h['mdelta_approx'].Fill(event.mdelta_approx)
    h['dww'].Fill(event.dww)
    h['dhw'].Fill(event.dhw)
    h['L1'].Fill(event.L1)
    h['L2'].Fill(event.L2)
    h['L3'].Fill(event.L3)
    h['L4'].Fill(event.L4)
    h['L5'].Fill(event.L5)
    h['B1'].Fill(event.B1)
    h['B2'].Fill(event.B2)
    h['B3'].Fill(event.B3)
    h['k'].Fill(event.k)

del f

#for param in params:
#    print 'processing parameter: %s' % param
#
#    bins = 100
#    if param == 'v1':
#        xmin = pow(10,-5)
#        xmax = pow(10,3)
#    elif param == 'v2':
#        xmin = 246.1
#        xmax = 246.3
#    elif param == 'v3':
#        xmin = pow(10,-5)
#        xmax = 0.35
#    elif param == 'mh1':
#        xmin = pow(10,-5)
#        xmax = 360
#    elif param == 'mh2':
#        xmin = pow(10,-5)
#        xmax = 2500
#    elif param == 'mh3':
#        xmin = pow(10,-5)
#        xmax = 4000
#    elif param == 'mA':
#        xmin = pow(10,-5)
#        xmax = 4000
#    elif param == 'mch':
#        xmin = pow(10,-5)
#        xmax = 4000
#    elif param == 'mdelta':
#        xmin = pow(10,-5)
#        xmax = 4000
##    elif param == 'dww':
##        xmin = pow(10,-6)
##        xmax = pow(10,1)
#    elif param == 'dhw':
#        xmin = pow(10,-5)
#        xmax = 4.8
#    else:
#        xmin = pow(10,-11)
#        xmax = pow(10,-8)
#
#    n = 'htmp(%i,%f,%f)' % (bins,xmin,xmax)
#    if t.Draw('%s>>%s' % (param, n), 
#              '', 
#              'goff'
#    ):
#        h[param].Add(ROOT.gDirectory.Get('htmp'), 1.)
#            
#del f
        
# HIST, PARAM CONFIG
for param in params:

    h[param].SetMarkerSize(1)
    h[param].SetMarkerStyle(ROOT.kDot)
    h[param].SetStats(0) 
    h[param].GetYaxis().SetTitleSize(20)
    h[param].GetYaxis().SetTitleFont(43)
    h[param].GetYaxis().SetTitleOffset(1.55)
        
    if param == 'v1': h[param].GetXaxis().SetTitle('v_{1} (GeV)')
    elif param == 'v2': h[param].GetXaxis().SetTitle('v_{2} (GeV)')
    elif param == 'v3': h[param].GetXaxis().SetTitle('v_{3} (GeV)')
    elif param == 'B1': h[param].GetXaxis().SetTitle('#Beta_{1} (GeV)')
    elif param == 'B2': h[param].GetXaxis().SetTitle('#Beta_{2} (GeV)')
    elif param == 'B3': h[param].GetXaxis().SetTitle('#Beta_{3} (GeV)')
    elif param == 'L1': h[param].GetXaxis().SetTitle('#lambda_{1} (GeV)')
    elif param == 'L2': h[param].GetXaxis().SetTitle('#lambda_{2} (GeV)')
    elif param == 'L3': h[param].GetXaxis().SetTitle('#lambda_{3} (GeV)')
    elif param == 'L4': h[param].GetXaxis().SetTitle('#lambda_{4} (GeV)')
    elif param == 'L5': h[param].GetXaxis().SetTitle('#lambda_{5} (GeV)')
    elif param == 'k': h[param].GetXaxis().SetTitle('#kappa (GeV)')
    elif param == 'mh1': h[param].GetXaxis().SetTitle('m_{h1} (GeV)')
    elif param == 'mh2': h[param].GetXaxis().SetTitle('m_{h2} (GeV)')
    elif param == 'mh3': h[param].GetXaxis().SetTitle('m_{h3} (GeV)')
    elif param == 'mA': h[param].GetXaxis().SetTitle('m_{A} (GeV)')
    elif param == 'mch': h[param].GetXaxis().SetTitle('m_{H^{#pm}} (GeV)')
    elif param == 'mdelta': h[param].GetXaxis().SetTitle('m_{#Delta} (GeV)')
    elif param == 'mA_approx': h[param].GetXaxis().SetTitle('m_{A} approx(GeV)')
    elif param == 'mch_approx': h[param].GetXaxis().SetTitle('m_{H^{#pm}} approx (GeV)')
    elif param == 'mdelta_approx': h[param].GetXaxis().SetTitle('m_{#Delta} approx (GeV)')
    elif param == 'dww': h[param].GetXaxis().SetTitle('#Gamma_{#Delta #rightarrow WW} (GeV)')
    elif param == 'dhw': h[param].GetXaxis().SetTitle('#Gamma_{#Delta #rightarrow HW} (GeV)')
    
    if param == 'v1' or param == 'v2' or param == 'mh2' or param == 'mh3' or param == 'mA' or param == 'mch' or param == 'mdelta' or param == 'mA_approx' or param == 'mch_approx' or param == 'mdelta_approx':
        h[param].GetXaxis().SetNdivisions(505)            

    
# TEXT CONFIG
xmin = 0.2
ymin = 0.18
tex = ROOT.TLatex(xmin, ymin, "Preliminary results")
tex.SetName('Test2')
tex.SetNDC(True)
tex.SetTextSize(0.04)

# PLOT IN CANVAS
for param in params:
    
    c = 'c_%s' % (param)
    c = ROOT.TCanvas('c', 'c', 800, 800)

    if doLogY == True: c.SetLogy()
    if doLogX == True: c.SetLogx()
    if param == 'dhw' or param == 'dww': c.SetLogx() 
    
    c.cd()
    h[param].Draw()
    tex.Draw()
                            
    c.Update()
    c.SaveAs('/home/saolivap/WorkArea/GitLab/HTM/util/plots/hists/%s.png' % (param))
