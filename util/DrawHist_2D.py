import ROOT 
ROOT.gROOT.SetBatch(True)

stylepath = '/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/'

ROOT.gROOT.LoadMacro( stylepath + 'AtlasStyle.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasLabels.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasUtils.C')
ROOT.SetAtlasStyle()
from ROOT import gPad

#Flags
doLogX = False
doLogY = True

basepath = '/home/saolivap/WorkArea/GitLab/HTM/build/'
tree = 'trip'
params = ['v1', 'v2', 'v3', 'mh1', 'mh2', 'mh3', 'mA', 'mch', 'mdelta', 'dww', 'dhw', 'L1', 'L2','L3', 'L4', 'L5', 'B1', 'B2', 'B3', 'k']
varias = ['mh1', 'mh2', 'mh3', 'mA', 'mch', 'mdelta']
plots = ['neutralMass', 'chargedMass']

sample = basepath + 'trip.root'

outfile = ROOT.TFile('/home/saolivap/WorkArea/GitLab/HTM/util/hist.root','RECREATE')

# CREATING HISTOGRAMS
h = {}
for param in params:
    h[param] = {}

    for varia in varias:
        h[param][varia] = {}

        bins = 1000
        if param == 'v1':
            xmin = pow(10,-5)
            xmax = pow(10,3)
        elif param == 'v2':
            xmin = 246.1
            xmax = 246.3
        elif param == 'v3':
            xmin = pow(10,-5)
            xmax = 0.35
        elif param == 'mh1':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'mh2':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'mh3':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'mA':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'mch':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'mdelta':
            xmin = pow(10,-5)
            xmax = 5000
        elif param == 'L5':
            xmin = -12
            xmax = 0
        elif param == 'k':
            xmin = pow(10,-10)
            xmax = 0.5*pow(10,-3)        
        elif param == 'dww':
            xmin = pow(10,-11)
            xmax = pow(10,-6)
        elif param == 'dhw':
            xmin = pow(10,-5)
            xmax = 4.8
        else:
            xmin = -1
            xmax = 1
            
        if varia == 'mh1':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mh2':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mh3':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mA':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mch':
            ymin = pow(10,-5)
            ymax = 5000
        elif varia == 'mdelta':
            ymin = pow(10,-5)
            ymax = 5000
        else:
            ymin = -1
            ymax = 1
            
        n = 'h_%s_%s' % (param, varia)
        h[param][varia] = ROOT.TH2F(n, n + "; %s (GeV); %s (GeV)" % (varia, param), bins, xmin, xmax, bins, ymin, ymax) 
        h[param][varia].SetDirectory(outfile)
    
# FILLING
f = ROOT.TFile.Open(sample)
t = f.Get(tree)

# For the moment we have to use this slow fill method (open the tree) because the direct (from below)
# does not have enough precision for the minimum value of a plotted variable (no more that 10-6)
for event in t:

    h['v1']['mh1'].Fill(event.v1,event.mh1)
    h['v1']['mh2'].Fill(event.v1,event.mh2)
    h['v1']['mh3'].Fill(event.v1,event.mh3)
    h['v1']['mA'].Fill(event.v1,event.mA)
    h['v1']['mch'].Fill(event.v1,event.mch)
    h['v1']['mdelta'].Fill(event.v1,event.mdelta)
    
    h['v3']['mh1'].Fill(event.v3,event.mh1)
    h['v3']['mh2'].Fill(event.v3,event.mh2)
    h['v3']['mh3'].Fill(event.v3,event.mh3)
    h['v3']['mA'].Fill(event.v3,event.mA)
    h['v3']['mch'].Fill(event.v3,event.mch)
    h['v3']['mdelta'].Fill(event.v3,event.mdelta)

    h['L1']['mh1'].Fill(event.L1,event.mh1)
    h['L1']['mh2'].Fill(event.L1,event.mh2)
    h['L1']['mh3'].Fill(event.L1,event.mh3)
    h['L1']['mA'].Fill(event.L1,event.mA)
    h['L1']['mch'].Fill(event.L1,event.mch)
    h['L1']['mdelta'].Fill(event.L1,event.mdelta)
    h['L2']['mh1'].Fill(event.L2,event.mh1)
    h['L2']['mh2'].Fill(event.L2,event.mh2)
    h['L2']['mh3'].Fill(event.L2,event.mh3)
    h['L2']['mA'].Fill(event.L2,event.mA)
    h['L2']['mch'].Fill(event.L2,event.mch)
    h['L2']['mdelta'].Fill(event.L2,event.mdelta)
    h['L3']['mh1'].Fill(event.L3,event.mh1)
    h['L3']['mh2'].Fill(event.L3,event.mh2)
    h['L3']['mh3'].Fill(event.L3,event.mh3)
    h['L3']['mA'].Fill(event.L3,event.mA)
    h['L3']['mch'].Fill(event.L3,event.mch)
    h['L3']['mdelta'].Fill(event.L3,event.mdelta)
    h['L4']['mh1'].Fill(event.L4,event.mh1)
    h['L4']['mh2'].Fill(event.L4,event.mh2)
    h['L4']['mh3'].Fill(event.L4,event.mh3)
    h['L4']['mA'].Fill(event.L4,event.mA)
    h['L4']['mch'].Fill(event.L4,event.mch)
    h['L4']['mdelta'].Fill(event.L4,event.mdelta)
    h['L5']['mh1'].Fill(event.L5,event.mh1)
    h['L5']['mh2'].Fill(event.L5,event.mh2)
    h['L5']['mh3'].Fill(event.L5,event.mh3)
    h['L5']['mA'].Fill(event.L5,event.mA)
    h['L5']['mch'].Fill(event.L5,event.mch)
    h['L5']['mdelta'].Fill(event.L5,event.mdelta)
    
    h['B1']['mh1'].Fill(event.B1,event.mh1)
    h['B1']['mh2'].Fill(event.B1,event.mh2)
    h['B1']['mh3'].Fill(event.B1,event.mh3)
    h['B1']['mA'].Fill(event.B1,event.mA)
    h['B1']['mch'].Fill(event.B1,event.mch)
    h['B1']['mdelta'].Fill(event.B1,event.mdelta)
    h['B2']['mh1'].Fill(event.B2,event.mh1)
    h['B2']['mh2'].Fill(event.B2,event.mh2)
    h['B2']['mh3'].Fill(event.B2,event.mh3)
    h['B2']['mA'].Fill(event.B2,event.mA)
    h['B2']['mch'].Fill(event.B2,event.mch)
    h['B2']['mdelta'].Fill(event.B2,event.mdelta)
    h['B3']['mh1'].Fill(event.B3,event.mh1)
    h['B3']['mh2'].Fill(event.B3,event.mh2)
    h['B3']['mh3'].Fill(event.B3,event.mh3)
    h['B3']['mA'].Fill(event.B3,event.mA)
    h['B3']['mch'].Fill(event.B3,event.mch)
    h['B3']['mdelta'].Fill(event.B3,event.mdelta)

    h['k']['mh1'].Fill(event.k,event.mh1)
    h['k']['mh2'].Fill(event.k,event.mh2)
    h['k']['mh3'].Fill(event.k,event.mh3)
    h['k']['mA'].Fill(event.k,event.mA)
    h['k']['mch'].Fill(event.k,event.mch)
    h['k']['mdelta'].Fill(event.k,event.mdelta)
    
del f
        
# HIST, PARAM CONFIG
for param in params:
    for varia in varias:

        h[param][varia].SetMarkerSize(1)
        h[param][varia].SetMarkerStyle(ROOT.kDot)
        h[param][varia].SetStats(0) 
        #h[param][varia].GetYaxis().SetTitleSize(20)
        #h[param][varia].GetYaxis().SetTitleFont(43)
        #h[param][varia].GetYaxis().SetTitleOffset(1.55)
        
#        if param == 'v1': h[param][varia].GetXaxis().SetTitle('v_{1} (GeV)')
#        elif param == 'v2': h[param][varia].GetXaxis().SetTitle('v_{2} (GeV)')
#        elif param == 'v3': h[param][varia].GetXaxis().SetTitle('v_{3} (GeV)')
#        elif param == 'B1': h[param][varia].GetXaxis().SetTitle('#Beta_{1} (GeV)')
#        elif param == 'B2': h[param][varia].GetXaxis().SetTitle('#Beta_{2} (GeV)')
#        elif param == 'B3': h[param][varia].GetXaxis().SetTitle('#Beta_{3} (GeV)')
#        elif param == 'L1': h[param][varia].GetXaxis().SetTitle('#lambda_{1} (GeV)')
#        elif param == 'L2': h[param][varia].GetXaxis().SetTitle('#lambda_{2} (GeV)')
#        elif param == 'L3': h[param][varia].GetXaxis().SetTitle('#lambda_{3} (GeV)')
#        elif param == 'L4': h[param][varia].GetXaxis().SetTitle('#lambda_{4} (GeV)')
#        elif param == 'L5': h[param][varia].GetXaxis().SetTitle('#lambda_{5} (GeV)')
#        elif param == 'k': h[param][varia].GetXaxis().SetTitle('#kappa (GeV)')
#        elif param == 'mh1': h[param][varia].GetXaxis().SetTitle('m_{h1} (GeV)')
#        elif param == 'mh2': h[param][varia].GetXaxis().SetTitle('m_{h2} (GeV)')
#        elif param == 'mh3': h[param][varia].GetXaxis().SetTitle('m_{h3} (GeV)')
#        elif param == 'mA': h[param][varia].GetXaxis().SetTitle('m_{A} (GeV)')
#        elif param == 'mch': h[param][varia].GetXaxis().SetTitle('m_{H^{#pm}} (GeV)')
#        elif param == 'mdelta': h[param][varia].GetXaxis().SetTitle('m_{#Delta} (GeV)')
#        elif param == 'dww': h[param][varia].GetXaxis().SetTitle('#Gamma_{#Delta #rightarrow WW} (GeV)')
#        elif param == 'dhw': h[param][varia].GetXaxis().SetTitle('#Gamma_{#Delta #rightarrow HW} (GeV)')

        if varia == 'mh1': h[param][varia].SetMarkerColor(ROOT.kRed)
        if varia == 'mh2': h[param][varia].SetMarkerColor(ROOT.kGreen)
        if varia == 'mh3': h[param][varia].SetMarkerColor(ROOT.kBlue)
        if varia == 'mA': h[param][varia].SetMarkerColor(ROOT.kMagenta)
        if varia == 'mch': h[param][varia].SetMarkerColor(ROOT.kCyan)
        if varia == 'mdelta': h[param][varia].SetMarkerColor(ROOT.kOrange)

#LEGENDS CONFIGURATION
g = {}
for varia in varias:
    g[varia] = {}
    n = 'h_%s' % (varia)
    g[varia] = ROOT.TH1F(n, n + "; %s (GeV); Events" % (varia), 200, 0, 5000)
    g[varia].SetDirectory(outfile)

    g[varia].SetMarkerSize(1)
    g[varia].SetMarkerStyle(ROOT.kFullCircle)
    
    if varia == 'mh1': g[varia].SetMarkerColor(ROOT.kRed)
    if varia == 'mh2': g[varia].SetMarkerColor(ROOT.kGreen)
    if varia == 'mh3': g[varia].SetMarkerColor(ROOT.kBlue)
    if varia == 'mA': g[varia].SetMarkerColor(ROOT.kMagenta)
    if varia == 'mch': g[varia].SetMarkerColor(ROOT.kCyan)
    if varia == 'mdelta': g[varia].SetMarkerColor(ROOT.kOrange)
# LEGENDS
xmin = 0.70
xmax = 0.85
ymin = 0.20
ymax = 0.40

leg_neuMass = ROOT.TLegend(xmin, ymin, xmax, ymax)
leg_neuMass.SetFillColor(0)
leg_neuMass.SetBorderSize(0)
leg_neuMass.SetTextFont(42)
leg_neuMass.SetTextSize(0.05)
leg_neuMass.SetName('Masses')
leg_neuMass.SetShadowColor(0)
leg_neuMass.AddEntry(g['mh1'],'m_{h1}', 'p')
leg_neuMass.AddEntry(g['mh2'],'m_{h2}', 'p')
leg_neuMass.AddEntry(g['mh3'],'m_{h3}', 'p')

leg_chMass = ROOT.TLegend(xmin, ymin, xmax, ymax)
leg_chMass.SetFillColor(0)
leg_chMass.SetBorderSize(0)
leg_chMass.SetTextFont(42)
leg_chMass.SetTextSize(0.05)
leg_chMass.SetName('Masses')
leg_chMass.SetShadowColor(0)
leg_chMass.AddEntry(g['mch'],'m_{H^{#pm}}', 'p')
leg_chMass.AddEntry(g['mdelta'],'m_{#Delta}', 'p')
leg_chMass.AddEntry(g['mA'],'m_{A}', 'p')
    
# TEXT CONFIG
xmin = 0.2
ymin = 0.2
tex = ROOT.TLatex(xmin, ymin, "Preliminary results")
tex.SetName('Test2')
tex.SetNDC(True)
tex.SetTextSize(0.04)
        
# PLOT IN CANVAS
for param in params:
    for plot in plots:
        print 'procesing plot: %s' %plot
                    
        #THStack
        h[plot] = ROOT.THStack('massStack','massStack')

        c = 'c_%s' % (plot)
        c = ROOT.TCanvas('c', 'c', 800, 800)
        if doLogY == True: c.SetLogy()
        if doLogX == True: c.SetLogx()    
        #c.cd()
        
        if plot == 'neutralMass':
            h[plot].Add(h[param]['mh1'])
            h[plot].Add(h[param]['mh2'])
            h[plot].Add(h[param]['mh3'])

        elif plot == 'chargedMass':
            h[plot].Add(h[param]['mch'])
            h[plot].Add(h[param]['mdelta'])
            h[plot].Add(h[param]['mA'])

        c = 'c_%s' % (plot)
        c = ROOT.TCanvas('c', 'c', 800, 800)
        if doLogY == True: c.SetLogy()
        if doLogX == True: c.SetLogx()    
        #c.cd()

        h[plot].Draw('nostack, hist')
        
        h[plot].GetYaxis().SetTitle('m_{H} (GeV)')
        if param == 'v1': h[plot].GetXaxis().SetTitle('v_{1} (GeV)')
        if param == 'v3': h[plot].GetXaxis().SetTitle('v_{3} (GeV)')
        if param == 'k':  h[plot].GetXaxis().SetTitle('#kappa (GeV)')
        if param == 'L1': h[plot].GetXaxis().SetTitle('#lambda_{1} (GeV)')
        if param == 'L2': h[plot].GetXaxis().SetTitle('#lambda_{2} (GeV)')
        if param == 'L3': h[plot].GetXaxis().SetTitle('#lambda_{3} (GeV)')
        if param == 'L4': h[plot].GetXaxis().SetTitle('#lambda_{4} (GeV)')
        if param == 'L5': h[plot].GetXaxis().SetTitle('#lambda_{5} (GeV)')
        if param == 'B1': h[plot].GetXaxis().SetTitle('#Beta_{1} (GeV)')
        if param == 'B2': h[plot].GetXaxis().SetTitle('#Beta_{2} (GeV)')
        if param == 'B3': h[plot].GetXaxis().SetTitle('#Beta_{3} (GeV)')
        
        if param == 'v1' or param == 'k':
            h[plot].GetXaxis().SetNdivisions(505)

        if plot == 'neutralMass':
            leg_neuMass.Draw()
        elif plot == 'chargedMass':
            leg_chMass.Draw()
            
        tex.Draw()
        
        c.Update()
        c.SaveAs('/home/saolivap/WorkArea/GitLab/HTM/util/plots/2D/%s_%s.png' % (plot,param))
        
