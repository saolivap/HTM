import ROOT
ROOT.gROOT.SetBatch(True)

#stylepath = '/home/saolivap/WorkArea/GitLab/AtlasStyle/'
stylepath = '/home/saolivap/WorkArea/GitLab/HTM/util/AtlasStyle/'

ROOT.gROOT.LoadMacro( stylepath + 'AtlasStyle.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasLabels.C')
ROOT.gROOT.LoadMacro( stylepath + 'AtlasUtils.C')
ROOT.SetAtlasStyle()
from ROOT import TF1,TLine

v3_rho = ROOT.TF1('v3_rho','(2*pow(x,2))/(pow(246,2) + 4*pow(x,2)) + 0.00037',0,3)
v3_rho.SetLineWidth(2)

#v1_v3 = ROOT.TF2('v1_v3','( 2*246*pow(x,2) ) / ( sqrt(pow(y,2)*(pow(246,2)+4*pow(x,2)) + 4*pow(246,2)*pow(x,4) + pow(246,4)*pow(x,2)) ) - pow(10,-6)',0,5000,0,2.5)
#v1_v3 = ROOT.TF1('v1_v3','2*246*pow(2.5,2)/sqrt(x) - pow(10,-6)',0,5000)
#v1_v3 = ROOT.TF2('v1_v3','x+y-1',0,1000,0,3)
v1_v3 = ROOT.TF2('v1_v3','x+y-1',-10,10,-10,10)
v1_v3.SetParameter(1,1)
v1_v3.SetLineWidth(2)

line = ROOT.TLine(0,0.0006,3,0.0006)
line.SetLineColor(ROOT.kGreen)
line.SetLineWidth(2)
line.SetLineStyle(2)

c1 = ROOT.TCanvas( 'c1', 'Example with Formula', 200, 10, 700, 500 )
c1.Update()
v3_rho.Draw()
line.Draw()
c1.SaveAs('/home/saolivap/WorkArea/GitLab/HTM/util/plots/func/v3_range.png')

c2 = ROOT.TCanvas( 'c2', 'Example with Formula', 200, 10, 700, 500 )
c2.Update()
v1_v3.Draw('cont4')
c2.SaveAs('/home/saolivap/WorkArea/GitLab/HTM/util/plots/func/v1_v3.png')


