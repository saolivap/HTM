// System include(s):
#include <iostream>

// Local include(s):
#include "TripModel.h"

// ROOT include(s):
#include "TTree.h"

int main( int argc, char* argv[] ) {

  int events = 1000000;

  boost::progress_timer t;  // start timing
  
  TripModel *trip = new TripModel(ALL_FREE);
  trip->addBranches();
  trip->execute(events);
  
  // Return gracefully:
  return 0;
}

