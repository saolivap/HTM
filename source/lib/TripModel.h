// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TRandom.h"
#include "TMatrix.h"
#include "TVector.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

class TripModel {

public:
  
  TripModel(Option option);
  ~TripModel();
  void addBranches();
  void initialize();
  void massMatrices();
  void decayRate();
  void neutrinoSector();
  void approximations();
  void execute(int n_events);
  float centreMassMom(float M0, float M1, float M2);

private:

  Option option_;
  int n_events_;
  TFile *file;
  TTree *tree;

  // model parameters
  Float_t v1;
  Float_t v2;
  Float_t v3;
  Float_t B1;
  Float_t B2;
  Float_t B3;
  Float_t L1;
  Float_t L2;
  Float_t L3;
  Float_t L4;
  Float_t L5;
  Float_t k;

  // Physics constants
  float pi = TMath::Pi();
  float Mz = 91.1876;
  float Mw = 80.425;
  float Me = 0.511*0.001;
  float Mu = 105.7*0.001;
  float Mt = 1.777;
  float fe = 2.9*0.000001, fu = 6.1*0.0001, ft = 1.0*0.01;

  // negative mass values counters
  int cpeven_mass_neg;
  int cpodd_mass_neg;
  int charged_mass_neg;

  // masses
  Float_t Mh1;
  Float_t Mh2;
  Float_t Mh3;
  Float_t Mj;
  Float_t Mg;
  Float_t MA;
  Float_t Mgc;
  Float_t Mch;
  Float_t Mdelta;

  // decays
  Float_t Dww;
  Float_t Dhw;
  Float_t Dee;
  Float_t Duu;
  Float_t Dtt;
  Float_t Deu;
  Float_t Det;
  Float_t Dut;

  // neutrino sector
  Float_t phi1;
  Float_t phi2;
  Float_t delta;
  Float_t s12;
  Float_t s23;
  Float_t s13;
  Float_t c12;
  Float_t c23;
  Float_t c13;
  Float_t m1;
  Float_t m2;
  Float_t m3;
  Float_t m21;
  Float_t m31;
  Float_t Mee;
  Float_t Muu;
  Float_t Mtt;
  Float_t Meu;
  Float_t Met;
  Float_t Mut;
  Float_t hee;
  Float_t huu;
  Float_t htt;
  Float_t heu;
  Float_t het;
  Float_t hut;

  // approximations
  Float_t MA_approx;
  Float_t Mch_approx;
  Float_t Mdelta_approx;

};

#endif
