// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "TripModel.h"

TripModel::TripModel(Option option)
{
  
  std::cout << std::endl;
  std::cout << "[TripModel :: TripModel] Generating model option " << option;

  option_ = option;

  if ( option_ == ALL_FREE ) std::cout << ": Free variation of all parameters" <<std::endl;
  if ( option_ == ALL_FIXED ) std::cout << ": All parameters fixed" <<std::endl;
  if ( option_ == V1_FREE ) std::cout << ": All parameters fixed, v1 free" <<std::endl;
  
}


TripModel::~TripModel()
{
  
  delete file;
  delete tree;
  
}
 
void TripModel::initialize()
{
  
  v1 = 0.0;
  v2 = 0.0;
  v3 = 0.0;
  B1 = 0.0;
  B2 = 0.0;
  B3 = 0.0;
  L1 = 0.0;
  L2 = 0.0;
  L3 = 0.0;
  L4 = 0.0;
  L5 = 0.0;
  k = 0.0;
  Mh1 = 0.0;
  Mh2 = 0.0;
  Mh3 = 0.0;
  Mj = 0.0;
  Mg = 0.0;
  MA = 0.0;
  Mgc = 0.0;
  Mch = 0.0;
  Mdelta = 0.0;
  MA_approx = 0.0;
  Mch_approx = 0.0;
  Mdelta_approx = 0.0;
  Dww = 0.0;
  Dhw = 0.0;
  Dee = 0.0;
  Duu = 0.0;
  Dtt = 0.0;
  Deu = 0.0;
  Det = 0.0;
  Dut = 0.0;
  phi1 = -999;
  phi2 = -999;
  delta = -999;
  s12 = -999;
  s23 = -999;
  s13 = -999;
  c12 = -999;
  c23 = -999;
  c13 = -999;
  m1 = 0;
  m2 = 0;
  m3 = 0;
  m21 = 0;
  m31 = 0;
  Mee = 0;
  Muu = 0;
  Mtt = 0;
  Meu = 0;
  Met = 0;
  Mut = 0;
  hee = 0;
  huu = 0;
  htt = 0;
  heu = 0;
  het = 0;
  hut = 0;
  cpeven_mass_neg = 0;
  cpodd_mass_neg = 0;
  charged_mass_neg = 0;
  
}

float TripModel::centreMassMom(float M0, float M1, float M2)
{

  // Decay of a parent particle (M0) to two daughter particles (M1 and M2)
  float Pcm = TMath::Sqrt( (pow(M0,2)-pow((M1+M2),2)) * (pow(M0,2)-pow((M1-M2),2)) );
  return Pcm;
  
}

  
void TripModel::addBranches()
{

  file = new TFile("trip.root", "RECREATE");
  tree = new TTree("trip", "Higgs Triplet Model");

  tree->Branch("v_{1}", &v1, "v1");
  tree->Branch("v_{2}", &v2, "v2");
  tree->Branch("v_{3}", &v3, "v3");
  tree->Branch("B_{1}", &B1, "B1");
  tree->Branch("B_{2}", &B2, "B2");
  tree->Branch("B_{3}", &B3, "B3");
  tree->Branch("#lambda_{1}", &L1, "L1");
  tree->Branch("#lambda_{2}", &L2, "L2");
  tree->Branch("#lambda_{3}", &L3, "L3");
  tree->Branch("#lambda_{4}", &L4, "L4");
  tree->Branch("#lambda_{5}", &L5, "L5");
  tree->Branch("#kappa", &k, "k");
  tree->Branch("m_{h1}", &Mh1, "mh1");
  tree->Branch("m_{h2}", &Mh2, "mh2");
  tree->Branch("m_{h3}", &Mh3, "mh3");
  tree->Branch("m_{J}", &Mj, "mj");
  tree->Branch("m_{G}", &Mg, "mg");
  tree->Branch("m_{A}", &MA, "mA");
  tree->Branch("m_{g#pm}", &Mgc, "mgc");
  tree->Branch("m_{H#pm}", &Mch, "mch");
  tree->Branch("m_{#Delta#pm#pm}", &Mdelta, "mdelta");
  tree->Branch("m_{A_approx}", &MA_approx, "mA_approx");
  tree->Branch("m_{H_approx}", &Mch_approx, "mch_approx");
  tree->Branch("m_{D_approx}", &Mdelta_approx, "mdelta_approx");
  tree->Branch("DWW", &Dww, "dww");
  tree->Branch("DHW", &Dhw, "dhw");
  tree->Branch("Dee", &Dee, "dee");
  tree->Branch("Duu", &Duu, "duu");
  tree->Branch("Dtt", &Dtt, "dtt");
  tree->Branch("Deu", &Deu, "deu");
  tree->Branch("Det", &Det, "det");
  tree->Branch("Dut", &Dut, "dut");  
  tree->Branch("#phi_{1}", &phi1, "phi1");
  tree->Branch("#phi_{2}", &phi2, "phi2");
  tree->Branch("#delta", &delta, "delta");
  tree->Branch("sin(12)", &s12, "s12");
  tree->Branch("sin(23)", &s23, "s23");
  tree->Branch("sin(13)", &s13, "s13");
  tree->Branch("cos(12)", &c12, "c12");
  tree->Branch("cos(23)", &c23, "c23");
  tree->Branch("cos(13)", &c13, "c13");
  tree->Branch("m_{1}", &m1, "m1");
  tree->Branch("m_{2}", &m2, "m2");
  tree->Branch("m_{3}", &m3, "m3");
  tree->Branch("m_{21}", &m21, "m21");
  tree->Branch("m_{31}", &m31, "m31");
  tree->Branch("M_{ee}", &Mee, "Mee");
  tree->Branch("M_{#mu#mu}", &Muu, "Muu");
  tree->Branch("M_{#tau#tau}", &Mtt, "Mtt");
  tree->Branch("M_{e#mu}", &Meu, "Meu");
  tree->Branch("M_{e#tau}", &Met, "Met");
  tree->Branch("M_{#mu#tau}", &Mut, "Mut");
  tree->Branch("h_{ee}", &hee, "hee");
  tree->Branch("h_{#mu#mu}", &huu, "huu");
  tree->Branch("h_{#tau#tau}", &htt, "htt");
  tree->Branch("h_{e#mu}", &heu, "heu");
  tree->Branch("h_{e#tau}", &het, "het");
  tree->Branch("h_{#mu#tau}", &hut, "hut");
  
}

void TripModel::massMatrices()
{
  
  // Charged matrix
  TMatrixDSym Charged(2);
  
  Charged[0][0] = k*pow(v2,2)*v1 / (2*v3) - L5*pow(v2,2) / 4;
  Charged[0][1] = sqrt(2)*L5*v2*v3 / 4 - sqrt(2)*k*v1*v2 / 2;
  Charged[1][0] = sqrt(2)*L5*v2*v3 / 4 - sqrt(2)*k*v1*v2 / 2;
  Charged[1][1] = k*v1*v3 - L5*pow(v3,2) / 2;
	
  const TMatrixDSymEigen eigen_charged(Charged);
  const TVectorD eignValCharged = eigen_charged.GetEigenValues();
  const TMatrixD eignVecCharged = eigen_charged.GetEigenVectors();

  // Neutral CP-even matrix
  TMatrixDSym CPeven(3);

  CPeven[0][0] = 2*B1*pow(v1,2) + k*pow(v2,2)*v3 / (2*v1);
  CPeven[0][1] = B2*v1*v2 - k*v2*v3;
  CPeven[0][2] = B3*v1*v3 - k*pow(v2,2) / 2;

  CPeven[1][0] = B2*v1*v2 - k*v2*v3;
  CPeven[1][1] = 2*L1*pow(v2,2);
  CPeven[1][2] = (L3+L5)*v3*v2 - k*v2*v1;

  CPeven[2][0] = B3*v1*v3 - k*pow(v2,2) / 2;
  CPeven[2][1] = (L3+L5)*v3*v2 - k*v2*v1;
  CPeven[2][2] = 2*(L2+L4)*pow(v3,2) + k*pow(v2,2)*v1 / (2*v3);

  const TMatrixDSymEigen eigen_even(CPeven);
  const TVectorD eignValEven = eigen_even.GetEigenValues();
  const TMatrixD eignVecEven = eigen_even.GetEigenVectors();

  // Neutral CP-Odd matrix  
  TMatrixDSym CPodd(3);
      
  CPodd[0][0] = k*v3*pow(v2,2) / (2*v1);
  CPodd[0][1] = k*v2*v3;
  CPodd[0][2] = k*pow(v2,2) / 2;

  CPodd[1][0] = k*v2*v3;
  CPodd[1][1] = 2*k*v1*v3;
  CPodd[1][2] = k*v1*v2;

  CPodd[2][0] = k*pow(v2,2) / 2;
  CPodd[2][1] = k*v1*v2;
  CPodd[2][2] = k*v1*pow(v2,2) / (2*v3);
  
  const TMatrixDSymEigen eigen_odd(CPodd);
  const TVectorD eignValOdd = eigen_odd.GetEigenValues();
  const TMatrixD eignVecOdd = eigen_odd.GetEigenVectors();

  // CP-Even Higgs masses - Mh1 (light SM Higgs-like), Mh2 and Mh3 (heavy CP-Even Higgs masses)
  if ( eignValEven[2] < 0 && eignValEven[1] < 0 && eignValEven[0] < 0 ) {
    ++cpeven_mass_neg;
  }
  else {
    Mh1 = sqrt(eignValEven[2]);
    Mh2 = sqrt(eignValEven[1]);
    Mh3 = sqrt(eignValEven[0]);
  }

  // CP-Odd Higgs masses - Mj (majoron mass), Mg (goldstone mass), MA (Higgs CP-Odd mass)
  if ( eignValOdd[2] < 0 && eignValOdd[1] < 0 && eignValOdd[0] < 0 ) {
    ++cpeven_mass_neg;
  }
  else {
    Mj = sqrt(eignValOdd[2]);
    Mg = sqrt(eignValOdd[1]);
    MA = sqrt(eignValOdd[0]);
  }

  // Charged Higgs masses - Mgc (charged goldstone mass), Mch (Charged Higgs mass)
  if ( eignValCharged[1] < 0 && eignValCharged[0] < 0 ) {
    ++charged_mass_neg;
  }
  else {
    Mgc = sqrt(eignValCharged[1]);
    Mch = sqrt(eignValCharged[0]);
  }

  // Double-charged Higgs mass (only one algebraic parameter)
  // should we propose that the Mdelta should be heavier than 0? It should be... seb
  Mdelta = sqrt( ( k*pow(v2,2)*v1 - L5*pow(v2,2)*v3 - 2*L4*pow(v3,3) ) / (2*v3) );
      
  // Masses calculated algebraically (we may need them later)
 
}

void TripModel::decayRate()
{

  // SU(2) g_2 coupling
  float g = sqrt( 4*pow(Mw,2) / (pow(v2,2)+2*pow(v3,2)) );
  float Cw = sqrt( ( pow(g,2)*(pow(v2,2)+4*pow(v3,2)) ) / (4*pow(Mz,2)) );

  // Double-charged Higgs decay

  // D++W+W+
  if( Mdelta > 2*Mw )  {
    Dww = ( centreMassMom(Mdelta,Mw,Mw)*pow(g,4)*pow(v3,2)*(3-pow(Mdelta,2)/pow(Mw,2)+pow(Mdelta,4)/(4*pow(Mw,4))) ) / ( 32*pi*pow(Mdelta,2) );
  }
  // probably put an else with Dww = 0

  // D++H+W+
  if( Mdelta > (Mw + Mch) )  {
    Dhw = ( pow(centreMassMom(Mdelta,Mch,Mw),3)*pow(g,2)*pow(v2,2) ) / ( 2*pi*pow(Mw,2)*(2*pow(v3,2)+pow(v2,2)) );
  }
  // probably put an else with Dhw = 0

  //D++tau+tau+
  if( Mdelta > 2*Mt ){
    Dtt = ( centreMassMom(Mdelta,Mt,Mt)*pow(htt,2)*(pow(Mdelta,2)-pow(Mt,2)-pow(Mt,2)-2*Mt*Mt)  ) / ( 4*pi*pow(Mdelta,2) );
  }	
    
  // Decay with errors
  // lambda function for the bosonic double charged Higgs 
  float LdD = pow(Mdelta,4) + pow(Mw,4) + pow(Mch,4) - 2*pow(Mdelta,2)*pow(Mch,2) - 2*pow(Mdelta,2)*pow(Mw,2)- 2*pow(Mch,2)*pow(Mw,2);
  // lamda functions for the leptonic LFV double charged Higgs
  float Ldeu = pow(Mdelta,4) + pow(Mu,4) + pow(Me,4) - 2*pow(Mdelta,2)*pow(Me,2) - 2*pow(Mdelta,2)*pow(Mu,2) - 2*pow(Me,2)*pow(Mu,2);
  float Ldet = pow(Mdelta,4) + pow(Mt,4) + pow(Me,4) - 2*pow(Mdelta,2)*pow(Me,2) - 2*pow(Mdelta,2)*pow(Mt,2) - 2*pow(Me,2)*pow(Mt,2);
  float Ldut = pow(Mdelta,4) + pow(Mu,4) + pow(Mt,4) - 2*pow(Mdelta,2)*pow(Mt,2) - 2*pow(Mdelta,2)*pow(Mu,2) - 2*pow(Mt,2)*pow(Mu,2);
      
  // // D++W+W+
  // if( Mdelta > 2*Mw )  {
  //   Dww = ( pow(g,4)*pow(v3,2)*sqrt(pow(Mdelta,2)-4*pow(Mw,2)) ) / (32*pi*pow(Mdelta,2));		
  // }
  // // probably put an else with Dww = 0
  
  // // D++H+W+
  // if( Mdelta > (Mw + Mch) )  {
  //   Dhw = ( pow(g,2)*pow(v2,2)*sqrt(LdD)*( pow( ( ( (2*pow(Mdelta,2)*sqrt(4*pow(Mw,2)*pow(Mdelta,2)+LdD) - LdD + sqrt(4*pow(Mw,2)*pow(Mdelta,2)+LdD) * sqrt(4*pow(Mch,2)*pow(Mdelta,2)+LdD) ) / (4*pow(Mdelta,2) ) ) / Mw ), 2) - pow(Mdelta,2) - pow(Mch,2) - sqrt(4*pow(Mch,2)*pow(Mdelta,2)+LdD) ) ) / ( 24*pi*Mdelta*(pow(v2,2) + 2*pow(v3,2)) * (sqrt(4*pow(Mw,2)*pow(Mdelta,2) + LdD ) + sqrt( 4*pow(Mch,2)*pow(Mdelta,2) + LdD) ) );
  // }

  // D++ee
  if( Mdelta > 2*Me ){
    Dee = pow(hee,2)*pow(Me,2)*sqrt( pow(Mdelta,2)-4*pow(Me,2) ) / 16*pi*pow(Mdelta,2);
  }

  //Delta->uu
  if( Mdelta > 2*Mu ){
    Duu = pow(huu,2)*pow(Mu,2)*sqrt( pow(Mdelta,2)-4*pow(Mu,2) ) / 16*pi*pow(Mdelta,2);
  }

  // //Delta->tt
  // if( Mdelta > 2*Mt ){
  //   Dtt = pow(htt,2)*pow(Mt,2)*sqrt( pow(Mdelta,2)-4*pow(Mt,2) ) / 16*pi*pow(Mdelta,2);
  // }	
	
  //Delta->eu
  if( Mdelta > (Me+Mu) && Ldeu > 0 ){
    Deu = ( sqrt(Ldeu)*pow(heu,2)*( sqrt(4*pow(Me,2)*pow(Mdelta,2)+Ldeu)*sqrt(4*pow(Mu,2)*pow(Mdelta,2)+Ldeu) - Ldeu+4*Me*Mu*pow(Mdelta,2) ) ) / ( 256*pi*pow(Mdelta,3)*( sqrt(4*pow(Me,2)*pow(Mdelta,2)+Ldeu) + sqrt(4*pow(Mu,2)*pow(Mdelta,2)+Ldeu) ) );
  }

  //Delta->et
  if( Mdelta > (Me+Mt) && Ldet > 0 ){
    Det = ( sqrt(Ldet)*pow(het,2)*( sqrt(4*pow(Me,2)*pow(Mdelta,2)+Ldet)*sqrt(4*pow(Mt,2)*pow(Mdelta,2)+Ldet) - Ldet+4*Me*Mt*pow(Mdelta,2) ) ) / ( 256*pi*pow(Mdelta,3)*( sqrt(4*pow(Me,2)*pow(Mdelta,2)+Ldet) + sqrt(4*pow(Mt,2)*pow(Mdelta,2)+Ldet) ) );
  }
  
  //Delta->ut
  if( Mdelta > (Mu+Mt) && Ldut > 0 ){
    Dut = ( sqrt(Ldut)*pow(hut,2)*( sqrt(4*pow(Mu,2)*pow(Mdelta,2)+Ldut)*sqrt(4*pow(Mt,2)*pow(Mdelta,2)+Ldut) - Ldut+4*Mu*Mt*pow(Mdelta,2) ) ) / ( 256*pi*pow(Mdelta,3)*( sqrt(4*pow(Mu,2)*pow(Mdelta,2)+Ldut) + sqrt(4*pow(Mt,2)*pow(Mdelta,2)+Ldut) ) ); 
  }
  
  
}

void TripModel::neutrinoSector()
{
  
  // neutrino Dirac+Majorana phases - to keep CP invariance they should take values 0, pi/2, pi, 3*pi/2
  phi1 = 0.0;
  phi2 = 0.0;
  delta = 0.0;

  // not diagonalize neutrino masses
  m2 = sqrt( pow(m21,2) + pow(m1,2) );
  m3 = sqrt( pow(m31,2) + pow(m1,2) );
    
  // cosine
  c13 = sqrt( 1 - pow(s13,2) );
  c23 = sqrt( 1 - pow(s23,2) );
  c12 = sqrt( 1 - pow(s12,2) );

  // Majorana matrix components
  Mee = sqrt(pow(m21,2))*pow(s12,2)*pow(c13,2)*cos(phi1) + sqrt(pow(m31,2))*pow(s13,2) + m1*cos(phi2)*cos(2*delta)*(pow(c12,2)*pow(c13,2) + pow(s12,2)*pow(c13,2)*cos(phi1) + pow(s13,2)*cos(phi2)*cos(2*delta) );

  Muu = sqrt(pow(m21,2))*cos(phi1)*pow(c12*c23-s12*s23*s13*cos(delta),2) + sqrt(pow(m31,2))*pow(s23,2)*pow(c13,2)*cos(phi2) + m1*( pow(-s12*c23-c12*s23*s13*cos(delta),2) + cos(phi1)*pow(c12*c23-s12*s23*s13*cos(delta),2) + pow(s23,2)*pow(c13,2)*cos(phi2) );

  Mtt = sqrt(pow(m21,2))*cos(phi1)*pow(-c12*s23-s12*c23*s13*cos(delta),2) + sqrt(pow(m3,2))*pow(c23,2)*pow(c13,2)*cos(phi2) + m1*( pow(s12*s23-c12*c23*s13*cos(delta),2) + cos(phi1)*pow(-c12*s23-s12*c23*s13*cos(delta),2) + pow(c23,2)*pow(c13,2)*cos(phi2) );

  Meu = sqrt(pow(m21,2))*(c12*c23-s12*s23*s13*cos(delta))*s12*c13*cos(phi1) + sqrt(pow(m31,2))*s23*c13*s13*cos(phi2)*cos(delta) + m1*( (-s12*c23-c12*s23*s13*cos(delta))*c12*c13 + (c12*c23-s12*s23*s13*cos(delta))*s12*c13*cos(phi1) + s23*c13*s13*cos(phi2)*cos(delta) );

  Met = sqrt(pow(m21,2))*(-c12*s23-s12*c23*s13*cos(delta))*s12*c13*cos(phi1) + sqrt(pow(m31,2))*c23*c13*s13*cos(delta)*cos(phi1) + m1*( (s12*s23-c12*c23*s13*cos(delta))*c12*c13 + (-c12*s23-s12*c23*s13*cos(delta))*s12*c13*cos(phi1) + c23*c13*s13*cos(delta)*cos(phi2) );

  Mut = sqrt(pow(m21,2))*(c12*c23-s12*s23*s13*cos(delta))*(-c12*s23-s12*c23*s13*cos(delta))*cos(phi1) + sqrt(pow(m31,2))*c23*s23*pow(c13,2)*cos(phi2) + m1*( (-s12*c23-c12*s23*s13*cos(delta))*(s12*s23-c12*c23*s13*cos(delta))+(c12*c23-s12*s23*s13*cos(delta))*(-c12*s23-s12*c23*s13*cos(delta))*cos(phi1) + c23*s23*pow(c13,2)*cos(phi2) );
  
  // Yukawa couplings 
  hee = Mee / (sqrt(2)*v3);
  huu = Muu / (sqrt(2)*v3);
  htt = Mtt / (sqrt(2)*v3);
  heu = Meu / (sqrt(2)*v3);
  het = Met / (sqrt(2)*v3);
  hut = Mut / (sqrt(2)*v3);

}

void TripModel::approximations()
{

  float g = sqrt( 4*pow(Mw,2) / (pow(v2,2)+2*pow(v3,2)) );

  // Higgs masses
  MA_approx = sqrt( ( k*pow(v2,2)*v1 ) / ( 2*v3 ) );
  Mch_approx = sqrt( pow(MA,2) - L5*pow(v2,2)/4 );
  Mdelta_approx = sqrt( pow(MA,2) - L5*pow(v2,2)/2 );
  
}

void TripModel::execute( int n_events )
{

  n_events_ = n_events;
  TRandom *rd = new TRandom();
  boost::progress_display show_progress( n_events_ );
  
  for (long int n_ev = 0; n_ev != n_events; n_ev++ )
    {
      
      // Model parameters
      if ( option_ == ALL_FREE )
	{
	  v1 = 1000 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  v2 = 246.218458;
	  v3 = 0.35 * ( rd->Rndm(n_ev) );
	  B1 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1); // was 0.0 + 2.0 
	  B2 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  B3 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  L1 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);		
	  L2 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  L3 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  L4 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1);    
	  L5 = -1.0 + 2.0 * fabs(2 * (rd->Rndm(n_ev)) - 1); // was -12.0 * fabs
	  k  = 0.0005 * (rd->Rndm(n_ev)); // really small value to keep a low mA mass (check other range)
	  // neutrino sector
	  s12 = sqrt( 0.19 + 0.26 * (rd->Rndm(n_ev)) );
	  s23 = sqrt( 0.27 + 0.46 * (rd->Rndm(n_ev)) );
	  s13 = sqrt( 0.033 * (rd->Rndm(n_ev)) );
	  m1  = pow(10,-5) * (rd->Rndm(n_ev));
	  m21 = sqrt( 6.7 * pow(10,-5) + 3.2 * pow(10,-5) * (rd->Rndm(n_ev)) );
	  m31 = sqrt( 0.85 * pow(10,-3) + 3.35 * pow(10,-3) * (rd->Rndm(n_ev)) );
	}
      else if ( option_ == ALL_FIXED )
	{
	  v1 = 500;
	  v2 = 246.218458;
	  v3 = 0.1;
	  B1 = 1.0;
	  B2 = 1.0;
	  B3 = 1.0;
	  L1 = 1.0;		
	  L2 = 1.0;
	  L3 = 1.0;
	  L4 = 1.0;    
	  L5 = -0.5;
	  k = 0.0002;
	  // neutrino sector
	  s12 = sqrt(0.32);
	  s23 = sqrt(0.5);
	  s13 = sqrt(0.033);
	  m1 = 0.0;
	  m21 = sqrt( 7.7*pow(10,-5) );
	  m31 = sqrt( 2.4*pow(10,-3) );
	}
      else if ( option_ == V1_FREE )
	{
	  v1 = 1000 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	  v2 = 246.218458;
	  v3 = 0.1;
	  B1 = 1.0;
	  B2 = 1.0;
	  B3 = 1.0;
	  L1 = 1.0;		
	  L2 = 1.0;
	  L3 = 1.0;
	  L4 = 1.0;    
	  L5 = -0.5;
	  k = 0.0002;
	  // neutrino sector
	  s12 = sqrt(0.32);
	  s23 = sqrt(0.5);
	  s13 = sqrt(0.033);
	  m1 = 0.0;
	  m21 = sqrt( 7.7*pow(10,-5) );
	  m31 = sqrt( 2.4*pow(10,-3) );
	}
      
      // calculate masses of the Higgs bosons
      massMatrices();

      // calculate HTM yukawa-neutrino
      neutrinoSector();

      // calculate decays rates
      decayRate();

      // calculate approximations
      approximations();

      // Fill all the branches with the parameters values
      tree->Fill();
      ++show_progress;
      
    }

  std::cout << "[TripModel :: Finalize] Number of negative CP-Even Higgs masses: " << cpeven_mass_neg <<std::endl;
  std::cout << "[TripModel :: Finalize] Number of negative CP-Odd Higgs masses: " << cpodd_mass_neg <<std::endl;
  std::cout << "[TripModel :: Finalize] Number of negative Charged Higgs masses: " << charged_mass_neg <<std::endl;

  // Print tree branches
  //  tree->Print();
  file->Write();

}



